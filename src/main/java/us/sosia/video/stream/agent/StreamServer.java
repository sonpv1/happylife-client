package us.sosia.video.stream.agent;

import java.awt.Dimension;
import java.net.InetSocketAddress;

import com.github.sarxos.webcam.Webcam;


public class StreamServer {

	/**
	 * @author kerr
	 * @param args
	 */
	public static void main(String[] args) {
//		String url = "ws://localhost:8080/happylife/livevideo";
		String url = "ws://hl-akz.rhcloud.com:8000/happylife/livevideo";
		Webcam.setAutoOpenMode(true);
		Webcam webcam = Webcam.getDefault();
		Dimension dimension = new Dimension(640, 480);
		webcam.setViewSize(dimension);

		StreamServerAgent serverAgent = new StreamServerAgent(webcam, url);
		serverAgent.start(new InetSocketAddress("localhost", 20000));
	}

}
