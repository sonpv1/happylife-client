package us.sosia.video.stream.agent;

import com.github.sarxos.webcam.Webcam;
import javafx.scene.image.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.sosia.video.stream.handler.H264StreamEncoder;

import javax.imageio.ImageIO;
import javax.websocket.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.SocketAddress;
import java.net.URI;
import java.util.concurrent.*;

@ClientEndpoint
public class StreamServerAgent implements IStreamServerAgent {
    protected final static Logger logger = LoggerFactory.getLogger(StreamServer.class);
    protected final    Webcam                   webcam;
    protected volatile boolean                  isStreaming;
    protected          ScheduledExecutorService timeWorker;
    protected          ExecutorService          encodeWorker;
    protected int FPS = 25;
    protected ScheduledFuture<?> imageGrabTaskFuture;
    private   Session            session;
    private String url;

    @OnOpen
    public void onOpen(Session session) {
        this.session = session;
    }

    @OnMessage
    public void onMessage(InputStream input) {
        System.out.println("WebSocket message Received!");
    }

    @OnClose
    public void onClose() {
        connectToWebSocket();
    }

    private void connectToWebSocket() {
        WebSocketContainer container = ContainerProvider.getWebSocketContainer();
        try {
            URI uri = URI.create(url);
            container.connectToServer(this, uri);
        } catch (DeploymentException | IOException ex) {
            logger.error("connectToWebSocket()" , ex);
            System.exit(-1);
        }
    }

    private void sendImage(BufferedImage image) {
        try (OutputStream output = session.getBasicRemote().getSendStream()) {
            ByteArrayOutputStream bas = new ByteArrayOutputStream();
            ImageIO.write(image, "png", bas);
            InputStream input = new ByteArrayInputStream(bas.toByteArray());

            byte[] buffer = new byte[1024];
            int read;
            while ((read = input.read(buffer)) > 0) {
                output.write(buffer, 0, read);
            }
            bas.flush();
            bas.close();
            input.close();
        } catch (IOException ex) {
            logger.error("SEND IMAGE", ex);
        }
    }

    public StreamServerAgent(Webcam webcam, String url) {
        super();
        this.url            = url;
        this.webcam         = webcam;
        this.timeWorker     = new ScheduledThreadPoolExecutor(1);
        this.encodeWorker   = Executors.newSingleThreadExecutor();
    }


    public int getFPS() {
        return FPS;
    }

    public void setFPS(int fPS) {
        FPS = fPS;
    }

    public void start(SocketAddress streamAddress) {
        connectToWebSocket();
        logger.info("Server started :{}", streamAddress);
        if (!isStreaming) {
            Runnable imageGrabTask = new ImageGrabTask();
            ScheduledFuture<?> imageGrabFuture =
                    timeWorker.scheduleWithFixedDelay(imageGrabTask,
                            0,
                            1000 / FPS,
                            TimeUnit.MILLISECONDS);
            imageGrabTaskFuture = imageGrabFuture;
            isStreaming = true;
        }
    }

    public void stop() {
        isStreaming = false;
    }

    protected volatile long frameCount = 0;

    private class ImageGrabTask implements Runnable {

        @Override
        public void run() {
            logger.info("image grabed ,count :{}", frameCount++);
            BufferedImage bufferedImage = webcam.getImage();
            encodeWorker.execute(new EncodeTask(bufferedImage));
        }

    }

    private class EncodeTask implements Runnable {
        private final BufferedImage image;

        public EncodeTask(BufferedImage image) {
            super();
            this.image = image;
        }

        @Override
        public void run() {
            try {
                if (null != image) {
                    System.out.println("SEND MSG");
                    sendImage(image);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}


	

