package net.crash;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamResolution;
import com.xuggle.mediatool.IMediaWriter;
import com.xuggle.mediatool.ToolFactory;
import com.xuggle.xuggler.ICodec;
import com.xuggle.xuggler.IPixelFormat;
import com.xuggle.xuggler.IVideoPicture;
import com.xuggle.xuggler.io.XugglerIO;
import com.xuggle.xuggler.video.ConverterFactory;
import com.xuggle.xuggler.video.IConverter;

import javax.imageio.ImageIO;
import javax.websocket.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URI;
import java.util.logging.Level;

/**
 * Created by sonphan on 10/22/15.
 */

@ClientEndpoint
public class Test {

    private Session session;

    @OnOpen
    public void onOpen(Session session) {
        this.session = session;
    }

    @OnMessage
    public void onMessage(InputStream input) {
        System.out.println("WebSocket message Received!");
//        Image image = new Image(input);
    }

    @OnClose
    public void onClose() {
        connectToWebSocket();
    }

    private void sendImage(BufferedImage image) {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        try (
                OutputStream output = session.getBasicRemote().getSendStream()) {
            ImageIO.write(image, "png", os);
            InputStream input = new ByteArrayInputStream(os.toByteArray());
            byte[] buffer = new byte[1024];
            int read;
            while ((read = input.read(buffer)) > 0) {
                output.write(buffer, 0, read);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void connectToWebSocket() {
        WebSocketContainer container = ContainerProvider.getWebSocketContainer();
        try {
            URI uri = URI.create("ws://localhost:8080/happylife/livevideo");
            container.connectToServer(this, uri);
        } catch (DeploymentException | IOException ex) {
            ex.printStackTrace();
            System.exit(-1);
        }
    }

    public void start() throws Exception {
        connectToWebSocket();
        ByteArrayOutputStream os     = new ByteArrayOutputStream();
        IMediaWriter          writer = ToolFactory.makeWriter(XugglerIO.map(os));
        Dimension             size   = WebcamResolution.QVGA.getSize();

        writer.addVideoStream(0, 0, ICodec.ID.CODEC_ID_H264, size.width, size.height);

        Webcam webcam = Webcam.getDefault();
        webcam.setViewSize(size);
        webcam.open(true);

        long start = System.currentTimeMillis();

        for (int i = 0; i < 50; i++) {

            System.out.println("Capture frame " + i);

            BufferedImage image = ConverterFactory.convertToType(webcam.getImage(), BufferedImage.TYPE_3BYTE_BGR);
            IConverter converter = ConverterFactory.createConverter(image, IPixelFormat.Type.YUV420P);
            IVideoPicture frame = converter.toPicture(image, (System.currentTimeMillis() - start) * 1000);
            frame.setKeyFrame(i == 0);
            frame.setQuality(0);

            writer.encodeVideo(0, frame);
            try (
                    OutputStream output = session.getBasicRemote().getSendStream()) {
                ImageIO.write(image, "png", os);
                InputStream input = new ByteArrayInputStream(os.toByteArray());
                byte[] buffer = new byte[1024];
                int read;
                while ((read = input.read(buffer)) > 0) {
                    output.write(buffer, 0, read);
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
//            sendImage(image);

            // 10 FPS
            Thread.sleep(100);
        }

//        writer.close();
    }

    public void sendVideo() {
        connectToWebSocket();
            File file = new File("output.ts");

        IMediaWriter writer = ToolFactory.makeWriter(file.getName());
        Dimension size = WebcamResolution.QVGA.getSize();

        writer.addVideoStream(0, 0, ICodec.ID.CODEC_ID_H264, size.width, size.height);

        Webcam webcam = Webcam.getDefault();
        webcam.setViewSize(size);
        webcam.open(true);

        long start = System.currentTimeMillis();
        try (
                OutputStream output = session.getBasicRemote().getSendStream()) {


            for (int i = 0; i < 50; i++) {

                System.out.println("Capture frame " + i);

                BufferedImage image = ConverterFactory.convertToType(webcam.getImage(), BufferedImage.TYPE_3BYTE_BGR);
                IConverter converter = ConverterFactory.createConverter(image, IPixelFormat.Type.YUV420P);

                IVideoPicture frame = converter.toPicture(image, (System.currentTimeMillis() - start) * 1000);
                frame.setKeyFrame(i == 0);
                frame.setQuality(0);

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write( image, "jpg", baos );
                baos.flush();
                byte[] imageInByte = baos.toByteArray();
                baos.close();
                output.write(imageInByte);
                writer.encodeVideo(0, frame);

                // 10 FPS
                Thread.sleep(100);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

//            writer.close();

        System.out.println("Video recorded in file: " + file.getAbsolutePath());
        }

    public static void main(String[] args) throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        final IMediaWriter writer = ToolFactory.makeWriter(XugglerIO.map(baos));
        Dimension size = WebcamResolution.QVGA.getSize();

        writer.addVideoStream(0, 0, ICodec.ID.CODEC_ID_H264, size.width, size.height);

        Webcam webcam = Webcam.getDefault();
        webcam.setViewSize(size);
        webcam.open(true);

        long start = System.currentTimeMillis();
            for (int i = 0; i < 50; i++) {

                System.out.println("Capture frame " + i);

                BufferedImage image = ConverterFactory.convertToType(webcam.getImage(), BufferedImage.TYPE_3BYTE_BGR);
                IConverter converter = ConverterFactory.createConverter(image, IPixelFormat.Type.YUV420P);

                IVideoPicture frame = converter.toPicture(image, (System.currentTimeMillis() - start) * 1000);
                frame.setKeyFrame(i == 0);
                frame.setQuality(0);

                writer.encodeVideo(0, frame);

                // 10 FPS
                Thread.sleep(100);
            }


            writer.close();

    }
}
