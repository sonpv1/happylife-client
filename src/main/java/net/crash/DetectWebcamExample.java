package net.crash;

import com.github.sarxos.webcam.Webcam;

/**
 * Created by sonphan on 10/23/15.
 */
public class DetectWebcamExample {
    public static void main(String[] args) {
        Webcam webcam = Webcam.getDefault();
        if (webcam != null) {
            System.out.println("Webcam: " + webcam.getName());
        } else {
            System.out.println("No webcam detected");
        }
    }
}
